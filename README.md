# Belajar Docker #

1. Membuat image

    ```
    docker build -t endymuhardin/belajar-docker .
    ```

2. Push image ke registry

    ```
    docker push endymuhardin/belajar-docker
    ```

3. Menjalankan docker image. Publish port 8080 di container ke port 12345 di host

    ```
    docker run -p 12345:8080 endymuhardin/belajar-docker
    ```

4. Browse ke [http://localhost:12345/waktu](http://localhost:12345/waktu)

## Menjalankan Database MySQL dengan Docker ##

```
docker run --name db-belajar-mysql -p 3306:3306 -v "$PWD/data-belajar-mysql:/var/lib/mysql" -e MYSQL_RANDOM_ROOT_PASSWORD=yes -e MYSQL_DATABASE=belajar-docker -e MYSQL_USER=belajar -e MYSQL_PASSWORD=docker --platform linux/amd64 --rm mysql:8
```

## Menjalankan Database PostgreSQL dengan Docker ##

```
docker run --name db-belajar-pgsql -p 5432:5432 -v "$PWD/data-belajar-pgsql:/var/lib/postgresql/data" -e POSTGRES_DB=belajar-docker -e POSTGRES_USER=belajar -e POSTGRES_PASSWORD=docker --rm postgres:14
```

## Menjalankan aplikasi dalam docker yang mengakses database ##

* MySQL

    ```
    docker run -p 12345:8080 -e "SPRING_DATASOURCE_URL=jdbc:mysql://192.168.52.44/belajar-docker" --rm endymuhardin/belajar-docker
    ```

* PostgreSQL

    ```
    docker run -p 12345:8080 -e "SPRING_DATASOURCE_URL=jdbc:postgresql://192.168.52.44/belajar-docker" --rm endymuhardin/belajar-docker
    ```

## Menjalankan Docker Compose ##

* Menjalankan docker-compose yang akan membaca file `docker-compose.yml`

    ```
    docker-compose up
    ```

* Menjalankan docker-compose dengan menyebutkan file yang akan dijalankan

    ```
    docker-compose -f docker-compose-db.yml
    docker-compose -f docker-compose-app.yml
    ```

## Mengubah Range IP default docker ##

* Edit file `daemon.json`. Untuk MacOS lokasinya ada di `${HOME}/.docker/daemon.json`
* Tambahkan setting `bip` dan `fixed-cidr` menjadi seperti ini

    ```json
    {
      "features": {
        "buildkit": true
      },
      "experimental": false,
      "builder": {
        "gc": {
          "defaultKeepStorage": "20GB",
          "enabled": true
        }
      },
      "bip": "192.168.1.5/24",
      "fixed-cidr": "192.168.1.5/25"
    }    
    ```

* Restart docker daemon

## Mengubah Range IP dalam Docker Compose ##

* Buat definisi network dengan range IP yang diinginkan (dalam file `docker-compose.yml`)

    ```yml
    networks:
      network_belajar:
        ipam:
          driver: default
          config:
            - subnet: 172.28.0.0/16
    ```

* Gunakan definisi network tersebut di masing-masing container/service

    ```yml
    services:
      db-belajar-postgresql:
        image: postgres:14
        volumes:
          - $PWD/data-belajar-pgsql:/var/lib/postgresql/data
        environment:
          - POSTGRES_DB=belajar-docker
          - POSTGRES_USER=belajar
          - POSTGRES_PASSWORD=docker
        networks:
          - network_belajar
    ```