create table customer (
    id varchar(36),
    name varchar (255) not null,
    email varchar (100) not null,
    primary key (id),
    unique(email)
);