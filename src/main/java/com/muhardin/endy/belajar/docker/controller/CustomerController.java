package com.muhardin.endy.belajar.docker.controller;

import com.muhardin.endy.belajar.docker.dao.CustomerDao;
import com.muhardin.endy.belajar.docker.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired private CustomerDao customerDao;

    @GetMapping("/")
    public Page<Customer> dataCustomer(Pageable page) {
        return customerDao.findAll(page);
    }
}
