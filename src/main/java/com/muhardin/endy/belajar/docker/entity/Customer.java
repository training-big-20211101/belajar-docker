package com.muhardin.endy.belajar.docker.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data @Entity
public class Customer {

    @Id
    private String id;
    private String name;
    private String email;
}
