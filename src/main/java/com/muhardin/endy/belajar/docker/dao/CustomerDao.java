package com.muhardin.endy.belajar.docker.dao;

import com.muhardin.endy.belajar.docker.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerDao extends PagingAndSortingRepository<Customer, String> {
}
