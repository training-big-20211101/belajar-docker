package com.muhardin.endy.belajar.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class InfoHostController {

    @GetMapping("/api/hostinfo")
    public Map<String, Object> hostInfo(HttpServletRequest request) {
        Map<String, Object> hasil = new LinkedHashMap<>();
        hasil.put("IP Aplikasi", request.getLocalAddr());
        return hasil;
    }
}
