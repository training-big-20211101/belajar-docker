package com.muhardin.endy.belajar.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@RestController
public class WaktuController {

    @GetMapping("/waktu")
    public Map<String, Object> sekarang() {
        Map<String, Object> hasil = new HashMap<>();
        hasil.put("waktu", LocalDateTime.now());
        return hasil;
    }
}
